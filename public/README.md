# From python2 to python3 @ALBA

## Tips for transitioning from python2 to python3

By **Tiago Coutinho** and **Carlos Pascual**

This presentation is available at: https://c-p.gitlab.io/slides_py2to3 ([source](https://gitlab.com/c-p/slides_py2to3))

---

# Why py3?

### Python2 is already dead:
- No longer updated (bugs & security issues are not fixed)
- Modules are no longer supporting it

### Python3 is better
- More efficient
- Cleanier, more consistent and easier code
- Provides many nice new features (new string formatting, )

---

# Which version to use?

### 2.6 vs 2.7 vs 3.5 vs **3.7** vs 3.8

The choice depends on which is the oldest type of machine in which you need to run the code...
- 2.6 only for prehistoric machines (e.g. SuSe 11)
- 2.7 only for ancient machines (e.g. SuSe 12)
- 3.5 only for old-ish machines (e.g. Debian 9)
- 3.7 for current (2020) machines (e.g. Debian 10) or with conda
- 3.8 same as 3.7, but some dependencies may not yet support it

---

# Which version to use?

### ALBA-specific dependencies

- Sardana:
  - sardana 2.x is py2-only
  - sardana 3.x is py3-only (py3.5+)
  
- Taurus:
  - Tau & Taurus < 4.5 are py2-only
  - Taurus 4.5 and 4.6 support both py2.7 and py3.5+
  - Taurus 4.7 may be py3-only
  
- fandango is py2-only
- pytangoarchiving is py2-only

- qwt is py2-only  (affects `TaurusPlot` and `TaurusTrend` implementations from 
`taurus.qt.qtgui.plot`, which are now replaced by `taurus_pyqtgraph` implementations)


---

# Which version to use?

### Note: Docs are versioned

Make sure that you read the appropriate one:

[https://docs.python.org/**3.8**/library/](https://docs.python.org/3.8/library/)

vs

[https://docs.python.org/**2.7**/library/](https://docs.python.org/2.7/library/)

---

# Differences between py2 and py3

http://python-future.org/compatible_idioms.html

- print function
- exceptions
- float division
- lists vs iterators
- str+unicode vs bytes+str
- imports
- compare with `None`
- ... 

---

# Differences between py2 and py3

**py2**: print is a statement (no parentheses)
```python
print "the number is" , 123
```

**py3**: print is a function
```python
print("the number is" , 123)
```

---

# Differences between py2 and py3

**py2**: comma syntax allowed in except
```python
try:
    foo()
except Exception, e
    print e
```

**py3**: 
```python
try:
    foo()
*except Exception as e
    print(e)
```

---

# Differences between py2 and py3

**py2**: `/` means integer division if used on ints
```python
a = 3 // 2  # --> a = 1
b = 3 / 2   # --> b = 1
c = 3 / 2.  # --> c = 1.5
d = 4 / 2   # --> d = 2
```

**py3**: `/` always means float division
```python
a = 3 // 2  # --> a = 1
*b = 3 / 2   # --> b = 1.5
c = 3 / 2.  # --> c = 1.5
d = 4 / 2   # --> d = 2.0
```

---

# Differences between py2 and py3

**py2**: `range()`, `dict.keys()`, etc return lists
```python
a = range(3)
print a  # --> [0, 1, 2]
a.append(9)  # a= [0, 1, 2, 9]
```

**py3**: `range`, `dict.keys()`, etc. do **not** return lists
```python
a = range(3)
print(a)  # --> range(0, 3)
a.append(9)  # --> AttributeError: 'range' object has no attribute 'append'
```

---

# Differences between py2 and py3

**py2**: map returns a list (it can be iterated many times)
```python
m = map(float, "1 2 3".split())  # --> map returns a list
for n in m:
    print n
s = 0
for n in m:
    s += n
print s  # --> s == 6
```

**py3**: map returns an iterator (which is spent after being used)
```python
m = map(float, "1 2 3".split())  # --> map returns an iterator
for n in m:
    print(n)
s = 0
for n in m:  # <-- loop exits immediately because m is already spent
    s += n
*print(s)  # --> s == 0  !!!  
```

---

# Differences between py2 and py3

**py2**: `str` is "bytes", (use `unicode` for "rich text")
```python
type("asd")   # --> str
type(b"asd")  # --> str
type(u"asd")  # --> unicode

f_in = open("in.csv", "rb"):
contents = f_in.read()
output = contents.replace(",", ";")
```

**py3**: `str` is "~unicode" (use `bytes` for "data")):
```python
type("asd")   # --> str
type(b"asd")  # --> bytes
type(u"asd")  # --> str

f_in = open("in.csv", "rb"):  
contents = f_in.read()  # contents is of type bytes
*output = contents.replace(b",", b";")
```

---

# Differences between py2 and py3

**py2**: allows implicit relative imports
```python
from mymodule import foo  # ambiguous: relative or absolute import?
import cPickle
```

**py3**: requires explicit relative imports. Module names are normalized
```python
from .mymodule import foo  # explicit relative import
import pickle  # renamed stdlib module
```

---

# Differences between py2 and py3

**py2**: None can be compared with numbers
```python
0 == None     # --> False
3 > None      # --> True
-1.23 < None  # --> False
```

**py3**: comparisons are stricter
```python
0 == None     # --> False
*3 > None      # --> TypeError: unorderable types: int() < NoneType()
*-1.23 < None  # --> TypeError: unorderable types: float() > NoneType()
```

---

# Differences between py2 and py3

### More details in:
### http://python-future.org/compatible_idioms.html

---

# Migrating existing py2 code to py3

- py3 or py2+3?
- Check your dependencies (make sure they all support py3)
- use git
- Auto-convert your code with `2to3`
- Run `autopep8` (or `black`)
- Optional but advised: add `setup.py` and use unit tests

---

# Migrating existing py2 code to py3

### py3 or py2+3?
- Unless **really** necessary, stick to py3 only
- If you need to support both, consider creating 2 branches and:
    - freeze the py2 version and develop only for py3
    - backport only essential changes to py2 (if at all)
- If you **insist** on supporting both py2+3 with the **same source**:
    - use the `future` module (read docs in https://python-future.org)

---

# Migrating existing py2 code to py3

### Check your dependencies 

- Make sure all the code that you depend on already supports py3
(or that you can replace it).

- Also make sure that the required versions of your dependencies 
are available in your target platforms.

---

# Migrating existing py2 code to py3

### use git

- simplifies the refactoring 
- helps with keeping a "py2 frozen branch"
- *tip for ALBA*: you can push your code to https://git.cells.es

---

# Migrating existing py2 code to py3

### Auto-convert your code with `2to3`

  1. Run `2to3 -w` in the base directory of your code
     - if using git avoid creating unnecessary backups by using also `-n`
  2. Review the changes (e.g. with `git diff`)
     - refer to the following docs to understand each change
       - https://docs.python.org/3/library/2to3.html#fixers
       - http://python-future.org/compatible_idioms.html
     - some automatic changes may be too conservative and can be refactored for keeping a cleaner code

---

# Migrating existing py2 code to py3

### Run `autopep8` or `black`

- Running an autoformatter can help fixing issues with whitespace that scape `2to3`
- As a collateral, your code will be more standards-compliant
- TIP: use either of the following commands in your project dir:
  - `autopep8 -i -r`
  - `black -tpy35 -l79`
    - the `-t` option makes black output compatible with python >= 3.5
- and... review the changes again !


---

# Migrating existing py2 code to py3

### Optional but advised: add `setup.py` and use unit tests

- add `setup.py` (tip: use [cookiecutter](https://github.com/audreyr/cookiecutter-pypackage))
- use unittests to spot regressions (tip: use [pytest](https://docs.pytest.org))



---

# Example 1  (original py2)

```python
from sardana.macroserver.macro import macro, Type
from mymodule import read_energy  # implicit relative import

@macro([["step_size", Type.Integer, 10, ""]])
def filter_range(self, step_size):
    """Returns a range of energies below the current energy"""

    full_range = range(1000 / step_size)  # float div in py3 (but range needs int)
    full_range.append(1000)  # does not work with range type

    try:
        max_ener = read_energy()
    except ValueError, e:  # comma syntax not allowed
        self.warning("Read problem: %r" % e)
        max_ener = None

    low_ener_range = []
    for p in full_range:
        if p < max_ener:  # illegal comparison in py3 if max_ener is None
            low_ener_range.append(p)

    return low_ener_range
```

---


# Example 1  (after 2to3)

```python
from sardana.macroserver.macro import macro, Type
from mymodule import read_energy  # implicit relative import

@macro([["step_size", Type.Integer, 10, ""]])
def filter_range(self, step_size):
    """Returns a range of energies below the current energy"""

*   full_range = list(range(1000 / step_size))  # float div in py3 (but range needs int)
*   full_range.append(1000)  # [FIXED] does not work with range type

    try:
        max_ener = read_energy()
*   except ValueError as e:  # [FIXED] comma syntax not allowed
        self.warning("Read problem: %r" % e)
        max_ener = None

    low_ener_range = []
    for p in full_range:
        if p < max_ener:  # illegal comparison in py3 if max_ener is None
            low_ener_range.append(p)

    return low_ener_range
```

---


# Example 1  (after manual review)

```python
from sardana.macroserver.macro import macro, Type
*from .mymodule import read_energy  # [FIXED] implicit relative import

@macro([["step_size", Type.Integer, 10, ""]])
def filter_range(self, step_size):
    """Returns a range of energies below the current energy"""

*   full_range = list(range(1000 // step_size))  # [FIXED] float div in py3 (but range needs int)
*   full_range.append(1000)  # [FIXED] does not work with range type

    try:
        max_ener = read_energy()
    except ValueError as e:  # [FIXED] comma syntax not allowed
        print("Read problem: %r" % e)  # [FIXED] print statement
        max_ener = None

    low_ener_range = []
    for p in full_range:
*       if max_ener is not None and p < max_ener:  # [FIXED] illegal comparison in py3 if max_ener is None
            low_ener_range.append(p)

    return low_ener_range
```

---


# Other transitions @ALBA

For some systems at ALBA, the py2 --> py3 transition may take place 
simultaneously with other transitions:


- Suse 11, 12 --> Debian 9
- Tango 7 --> Tango 9
- PyQt4 --> PyQt5
- Taurus 3 --> Taurus 4
- Sardana 2 --> Sardana 3

---


# Other transitions @ALBA

### Suse 11, 12 --> Debian 9

- More modern (and maintained) system
- More (and newer) applications available (e.g. docker, jupyter, ...)
- Some old libraries or unmaintained applications may not be available
- Some "manual" environment configurations may need adjusting

---


# Other transitions @ALBA

### Tango 7 --> Tango 9

- No notifd. Replaced by zeromq
- Some new features available (pipes, etc)
- Mixed versions between client and servers are possible but there may be some issues
- In BLs, the `TANGO_HOST` may need changing to tblXX  (instead of tblXX01)
- **Until bug #292 in PyTango is solved**, use `tango` instead of `taurus` for reading Tango attributes



---


# Other transitions @ALBA

### PyQt4 --> Pyqt5

- Translate GUIs to use "new-style signals"
- Remove remnants of sip API1 (`QVariant`, `QString`, `QStringList`,...)
- PyQwt5-based widgets no longer available. Replace by pyqtgraph-based widgets
- Re-create `.ui` files that use deprecated widgets
- Load `.ui` files dynamically with `uic.loadUI` instead of compiling them with `pyuic4`

See https://github.com/taurus-org/taurus/wiki/Best-Practices-for-Taurus-4

---

# Other transitions @ALBA

### Taurus 3 --> Taurus 4.6

- Be aware of [differences between Taurus States and Tango States](https://github.com/taurus-org/taurus/wiki/Best-Practices-for-Taurus-4#taurus-state-vs-tango-state)
- Do not use icon resource files. Use the [new Icon API instead](http://taurus-scada.org/devel/icon_guide.html)
- Use `QT_API` environment variable to select between `pyqt4` and `pyqt5` 
- Migrate usage of qwt5-based plots and trends to [taurus_pyqtgraph](https://github.com/taurus-org/taurus_pyqtgraph)-based implementations
- Adapt to attributes returning `Quantity` objects instead of scalars

See http://taurus-scada.org/devel/taurus3to4.html
See https://github.com/taurus-org/taurus/wiki/Best-Practices-for-Taurus-4
---


# Other transitions @ALBA

### Taurus 3 --> Taurus 4.6

- Use the new `taurus` (and `taurus-py2`) commands:
    - `tauruspanel` --> `taurus panel`
    - `taurusform` --> `taurus form`
    - `taurusplot` --> `taurus tpg plot` or `QT_API=pyqt4 taurus-py2 qwt5 plot` 
    - `taurustrend` --> `taurus tpg trend` or `QT_API=pyqt4 taurus-py2 qwt5 trend` 
    - `taurustrend2d` --> `taurus guiqwt trend2d`
    - `taurusimage` --> `taurus guiqwt image`
    - `taurusconfigbrowser` --> `taurus config`
    - `taurusdesigner` --> `taurus designer`
    - `taurusgui --new-gui` --> `taurus newgui`
    - many more see `taurus --help`
    
---


# Other transitions @ALBA

### Sardana 2 --> Sardana 3

- Sardana servers run on `ctblXXsard01` e.g. `ctbl09sard01` but in BL01 where
  they run on `ibl0104`. In all cases the user that runs the processes is `tangosys`.

- Showscan online now uses pyqtgraph instead of qwt. There are some look-and-feel
  issues that will be solved soon. 

- GUIs containing MacroButton  can not run on Python 2 anymore

- **Recipies to overcome #292 in PyTango in macros**: 
    - use Sardana elements cache stored in MacroServer for reading Sardana
      elements' attributes:

    ```python
    # in macros, for Sardana elements, instead of
    taurus.Attribute("theta/position").read().rvalue.magnitude
    # or
    taurus.Device("theta").getAttribute("position").read().rvalue.magnitude
    # use Sardana elements stored in MacroServer cache
    self.getMoveable("theta").getPosition() 
    ```
    - use `tango` instead of `taurus` for reading Tango (not proceeding from
    Sardana) attributes:

    ```python
    # in macros, for arbitrary Tango attributes, instead of
    taurus.Attribute("bl04/ct/eps-plc-01/fe_open").read().rvalue.magnitude
    # or
    taurus.Device("bl04/ct/eps-plc-01").getAttribute("fe_open").read().rvalue.magnitude
    # use tango 
    tango.DeviceProxy("bl04/ct/eps-plc-01").read_attribute("fe_open").value 
    ```




