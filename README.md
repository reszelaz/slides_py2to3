# py2 to py3 @ ALBA

This project provides slides for a presentation at ALBA synchrotron

The slides are written in markdown and rendered with [remark.js](https://github.com/gnab/remark)

See the [slides](https://c-p.gitlab.io/slides_py2to3) or their [source](public/) 

You can render them locally with:

```console
git clone https://gitlab.com/c-p/slides_py2to3
cd slides_py2to3/public/
python3 -m http.server
```

... and view them by opening `http://localhost:8000` in your web browser
